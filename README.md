# Matlab-CAP
## Summary
This is an incomplete client for [DENSO b-CAP communication protocol](https://www.densorobotics-europe.com/en/product/b-cap-1) written in Matlab.
The main functionality as connect/disconnect from the robot, take the control of the arm, get/release a variable handler or get/set the value of a variable are available.

## Usage
1. Clone the repository in your matlab project root.
   ```
   git clone https://gitlab.com/ccatterina/matlabcap.git {{project_path}}
   ```
2. Usage example (from your matlab shell or in your matlab script):
   ```matlab
   % ip: IP Address of the robot controller. [string]
   % port: TCP port of the robot controller (usually 5007). [int]
   % robot_name: The name of the robot. [string]
   robot = Bcap(ip, port, robot_name);

   robot.connect();
   ext_speed = robot.current_ext_speed();
   fprintf('The current external speed of the robot is %f.\n', ext_speed);

   robot.disconnect();
   ```

For a full list of available functions see [functions documentation](https://gitlab.com/ccatterina/matlabcap/tree/master/docs/developed_functions.md).

## Develop missing BCap functions.
You can contribute to this project developing the missing BCap functions (see docs/bcap_doc.pdf for the commands list).


To simplify the implementation of a new function you can use the following methods:

- **`function [packet] = create_bcap_packet(obj, fcode, serial, varargin)`**

  Arguments:
    - *fcode*: Bcap Function code [int]
    - *serial*: A sequential serial number for the packet. [int]
    - *vartype*: The variable type (See bcap properties). [int]
    - *var*: The variable value. [any]

    For each packet variable a couple (vartype, var) must be specified.

  Returns:
    - The bcap packet. [array of uints]

- **`function [args] = decode_bcap_packet(obj, packet, serial)`**

  Arguments:
    - *packet*: Bcap packet to decode. [array_of uints]
    - *serial*: A sequential serial number for the packet. [int]

  Returns:
    - Variables included in the response packet. [array]

- **`function response = send_and_recv(obj, request)`**

  Arguments:
    - *request*: Bcap request packet [array of uint8]

  Returns:
    - The Bcap response packet. [array of uint8]
