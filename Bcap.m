classdef Bcap < handle
    properties(Constant)
        VT_EMPTY = 0
        VT_NULL = 1
        VT_ERROR = 10
        VT_UI1 = 17
        VT_I2 = 2
        VT_UI2 = 18
        VT_I4 = 3
        VT_UI4 = 19
        VT_R4 = 4
        VT_R8 = 5
        VT_BSTR = 8
        VT_BOOL = 11
        VT_ARRAY_UI1 = 8209
        VT_ARRAY_I2 = 8194
        VT_ARRAY_UI2 = 8210
        VT_ARRAY_I4 = 8195
        VT_ARRAY_UI4 = 8211
        VT_ARRAY_R4 = 8196
        VT_ARRAY_R8 = 8198
        VT_ARRAY_BOOL = 8203
        VLENGTH = containers.Map([0 1 10 17 18 19 2 3 4 5 8 11] , ...
                                 {0, 0, 0, 1, 2, 4, 2, 4, 4, 8, 0, 1})
        S_OK = 0
        ERRORS = containers.Map([hex2dec('80004001'), hex2dec('80004004'), ...
                                 hex2dec('80004005'), hex2dec('80070005'), ...
                                 hex2dec('80070006'), hex2dec('8007000E'), ...
                                 hex2dec('80070057'), hex2dec('8000FFFF')], ...
                                {'Not Implemented', 'Function interrupted', ...
                                 'Function failed', 'Access failed', ...
                                 'Illegal Handle', 'Memory shortage', ...
                                 'Illegal argument', 'Fatal error occured'})
    end
    properties
        ip
        port
        robot_name
        socket
        serial
        controller
        robot_handler
    end
    methods
        function obj = Bcap(ip, port, robot_name)
            obj.ip = ip;
            obj.port = port;
            obj.robot_name = robot_name;
            obj.serial = 0;
            obj.controller = 0;
            obj.robot_handler = 0;
            obj.socket = tcpip(ip, port, 'NetworkRole', 'client');
        end

        % COMPLEX FUNCTIONS
        
        function disconnect(obj)
            % Disconnects controller, stops service and closes
            % the socket.
            obj.controller_disconnect(obj.controller);
            obj.service_stop();
            obj.serial = 0;
            obj.controller = 0;
            obj.robot_handler = 0;
            fclose(obj.socket);
        end
                
        function connect(obj)
            % Opens the socket, starts service, gets controller and
            % and robot handles.
            fopen(obj.socket);
            
            % The following two commands are copied from the bcap tester,
            % they are not documented but they seems to be necessary.
            packet = obj.create_bcap_packet(4294967291, obj.serial, Bcap.VT_EMPTY, '');
            obj.send_and_recv(packet);
            obj.serial = obj.serial + 1;
            packet = obj.create_bcap_packet(4294967295, obj.serial, Bcap.VT_I4, 2);
            obj.send_and_recv(packet);
            obj.serial = obj.serial + 1;
            
            obj.service_start();
            obj.controller = obj.controller_connect();
            obj.robot_handler = obj.get_robot(obj.controller);
        end
        
        function takearm(obj)
            packet = obj.create_bcap_packet(64, obj.serial, Bcap.VT_I4, obj.robot_handler, ...
                                             Bcap.VT_BSTR, 'Takearm', Bcap.VT_ARRAY_I4, [0,1]);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end

        function change_ext_speed(obj, speed)
            % Changes external speed sending a Robot_Execute(64) request
            % with 'ExtSpeed' as command name.
            % Arguments:
            %   - speed: Speed value to set (0-100). [single]
            packet = obj.create_bcap_packet(64, obj.serial, Bcap.VT_I4, obj.robot_handler, ...
                                            Bcap.VT_BSTR, 'ExtSpeed', Bcap.VT_R4, speed);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end

        function pos = get_current_pos(obj)
            % Changes external speed sending a Robot_Execute(64) request
            % with 'ExtSpeed' as command name.
            % Arguments:
            %   - speed: Speed value to set (0-100). [single]
            packet = obj.create_bcap_packet(64, obj.serial, Bcap.VT_I4, obj.robot_handler, ...
                                             Bcap.VT_BSTR, 'CurPos', Bcap.VT_EMPTY, '');
            response = obj.send_and_recv(packet);
            pos = obj.decode_bcap_packet(response, obj.serial);
            pos = pos(1:3);
            obj.serial = obj.serial + 1;
        end
        
        function speed = current_ext_speed(obj)
            % Returns current external speed.
            % Returns:
            %   - speed: Current external speed. [single]
            var_handle = obj.robot_get_variable('@EXTSPEED');
            speed = obj.get_value(var_handle);
            obj.release_var(var_handle);
        end
                
        %%%%%%%%%% BASIC FUNCTIONS %%%%%%%%%%%
        function service_start(obj)
            % Sends Service_Start(1) request.
            packet = obj.create_bcap_packet(1, obj.serial);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end

        function service_stop(obj)
            % Sends Service_Stop(2) request.
            packet = obj.create_bcap_packet(2, obj.serial);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end
        
        function controller = controller_connect(obj)
            % Sends Controller_Connect(3) request. 
            % Returns
            %   - controller: Controller handle. [int16]
            packet = obj.create_bcap_packet(3, obj.serial, Bcap.VT_BSTR, 'b-CAP', ...
                                             Bcap.VT_BSTR, 'CaoProv.DENSO.VRC', ...
                                             Bcap.VT_BSTR, obj.ip, Bcap.VT_BSTR, 'WPJ=*');
            response = obj.send_and_recv(packet);
            controller = obj.decode_bcap_packet(response, obj.serial);
            controller = controller(1);
            obj.serial = obj.serial + 1;
        end
        
        function controller_disconnect(obj, controller)
            % Sends Controller_Disconnect(4) request. 
            packet = obj.create_bcap_packet(4, obj.serial, Bcap.VT_I4, controller);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end
        
        function robot_handle = get_robot(obj, controller)
            % Sends Controller_GetRobot(7) request. 
            % Returns
            %   - robot_handle: Robot handle. [int32]
            packet = obj.create_bcap_packet(7, obj.serial, Bcap.VT_I4, controller, ...
                                             Bcap.VT_BSTR, obj.robot_name, ...
                                             Bcap.VT_BSTR, '$IsIDHandle$');
            response = obj.send_and_recv(packet);
            robot_handle = obj.decode_bcap_packet(response, obj.serial);
            robot_handle = robot_handle(1);
            obj.serial = obj.serial + 1;
        end

        function var_handle = robot_get_variable(obj, var_name)
            % Get robot variable handle, sending a Robot_GetVariable(62)
            % request.
            % Arguments:
            %   - var_name: Variable name. [string]
            % Returns:
            %   - var_handle: Handle of the variable. [int32]
            packet = obj.create_bcap_packet(62, obj.serial, Bcap.VT_I4, obj.robot_handler, ...
                                             Bcap.VT_BSTR, var_name, Bcap.VT_EMPTY, '');
            response = obj.send_and_recv(packet);
            var_handle = obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end
 
        function robot_speed(obj, speed)
            % Changes internal speed sending a Robot_Speed(74) request
            % Arguments:
            %   - speed: Speed value to set (0-100). [single]
            packet = obj.create_bcap_packet(74, obj.serial, Bcap.VT_I4, ...
                                             obj.robot_handler, Bcap.VT_I4, ...
                                             0, Bcap.VT_R4, speed);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end
        
        function value = get_value(obj, var_handle)
            % Get value of the specified variable, Variable_GetValue(101)
            % request.
            % Arguments:
            %   - var_handle: Variable handle. [int]
            % Returns:
            %   - value: Variable value. [depends on variable type]
            packet = obj.create_bcap_packet(101, obj.serial, Bcap.VT_I4, var_handle);
            response = obj.send_and_recv(packet);
            value = obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end
        
        function put_value(obj, var_handle, var_type, value)
            % Write value to specified variable, Variable_PutValue(102)
            % request.
            % Arguments:
            %   - var_handle: Variable handle. [int]
            %   - var_type: Variable type. [int]
            %   - value: Value to write. [depends on variable type]
            packet = obj.create_bcap_packet(102, obj.serial, Bcap.VT_I4, var_handle, ...
                                             var_type, value);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end

        function release_var(obj, var_handle)
            % Release variable handle, Variable_Release(111) request.
            % Arguments:
            %   - var_handle: Variable handle. [int]
            packet = obj.create_bcap_packet(111, obj.serial, Bcap.VT_I4, var_handle);
            response = obj.send_and_recv(packet);
            obj.decode_bcap_packet(response, obj.serial);
            obj.serial = obj.serial + 1;
        end
        
        %%%%% CORE METHODS %%%%%
        
        function response = send_and_recv(obj, request)
            % Sends the bCAP request, returns the bCAP response.
            % Arguments:
            %   - request: bCAP request packet. [array of uint8]
            % Returns
            %   - response: bCAP response packet. [array of uint8]
            fwrite(obj.socket, request, 'uint8');
            while obj.socket.BytesAvailable == 0
                pause(0.01);
            end
            response = fread(obj.socket, obj.socket.BytesAvailable, 'uint8');
        end  
        function [arg] = arg_to_bytes(~, vartype, var)
            % encodes a variable of a packet to a vector of uints
            %
            % Arguments:
            %   - vartype: The variable type (See Bcap properties). [int]
            %   - var: Variable value. [any]
            % Returns:
            %   - an array of uints that represents the var in the bcap
            %   packet.
            switch vartype
                case {Bcap.VT_EMPTY, Bcap.VT_NULL}
                    v = [];
                case {Bcap.VT_I2, Bcap.VT_ARRAY_I2}
                    v = int16(var);
                case {Bcap.VT_I4, Bcap.VT_ARRAY_I4, Bcap.VT_ERROR}
                    v = int32(var);
                case {Bcap.VT_R4, Bcap.VT_ARRAY_R4}
                    v = single(var);
                case {Bcap.VT_R8, Bcap.VT_ARRAY_R8}
                    v = double(var);
                case {Bcap.VT_UI2, Bcap.VT_ARRAY_UI2, Bcap.VT_BSTR}
                    v = uint16(var);
                case {Bcap.VT_UI4, Bcap.VT_ARRAY_UI4}
                    v = uint32(var);
                case {Bcap.VT_BOOL, Bcap.VT_ARRAY_BOOL}
                    v = boolean(var);
                otherwise
                    throw(MException('BCAP:InvalidVartype', 'Invalid variable type'));
            end
            arg = typecast(v, 'uint8');
            if vartype == Bcap.VT_BSTR
                arg = [typecast(uint32(length(arg)), 'uint8'), arg];
            end
            return
        end
        function [arg] = bytes_to_arg(~, vartype, bytes)
            % decodes bcap packet variable
            %
            % Arguments:
            %   - vartype: The variable type (See Bcap properties). [int]
            %   - bytes: array of uints that represents the variable in the
            %            bcap packet. [array of uint]
            % Returns:
            %   - Variable value. [any]
            v = uint8(bytes);
            switch vartype
                case {Bcap.VT_EMPTY, Bcap.VT_NULL}
                    arg = [];
                case Bcap.VT_I2
                    arg = typecast(v, 'int16');
                case {Bcap.VT_I4, Bcap.VT_ERROR}
                    arg = typecast(v, 'int32');
                case Bcap.VT_R4
                    arg = typecast(v, 'single');
                case Bcap.VT_R8
                    arg = typecast(v, 'double');
                case Bcap.VT_UI2
                    arg = typecast(v, 'uint16');
                case Bcap.VT_BSTR
                    arg = typecast(v, 'uint16');
                    arg = string(char(arg));
                case Bcap.VT_UI4
                    arg = typecast(v, 'uint32');
                case Bcap.VT_BOOL
                    arg = typecast(v, 'boolean');
                otherwise
                    throw(MException('BCAP:InvalidVartype', 'Invalid variable type'));
            end
            return
        end
        function [ packet ] = create_bcap_packet(obj, fcode, serial, varargin)
            % Creates a Bcap Packet.
            %
            % Arguments:
            %   - fcode: Bcap Function code. [int]
            %   - serial: A sequential serial number for the packet. [int]
            %   - vartype: The variable type (See Bcap properties). [int]
            %   - var: The variable value. [any]
            %   For each packet variable a couple (vartype, var) must be
            %   specified.
            %
            % Returns:
            % The bcap packet. [array of uints]
            soh = [uint8(1)];
            serial = typecast(uint16(serial), 'uint8');
            version = [uint8(1), uint8(0)];
            fcode = typecast(uint32(fcode), 'uint8');
            nvar = (nargin - 3) / 2;
            nargs = typecast(uint16(nvar), 'uint8');
            args = [];
            for i=1:2:nvar*2
                arg_type = typecast(uint16(varargin{i}), 'uint8');
                % If arg_type is greater than 8192 the argument is an
                % array.
                if varargin{i} >= 8192
                    arr_len = length(varargin{i + 1});
                else
                    arr_len = 1;
                end
                arr_len = typecast(uint32(arr_len), 'uint8');
                val = obj.arg_to_bytes(varargin{i}, varargin{i+1});
                arg_len = typecast(uint32(2 + 4 + length(val)), 'uint8');
                args = [args, arg_len, arg_type, arr_len, val];
            end
            mlen = typecast(uint32(1 + 4 + 2 + 2 + 4 + 2 + length(args) + 1 + 1), 'uint8');
            mode = [0]; % Mode 0: Uncompressed
            eot = [4];
            packet = [soh, mlen, serial, version, fcode, nargs, args, mode, eot];
            return
        end
        function [ args ] = decode_bcap_packet(obj, packet, serial)
            % Decodes a bcap packet and raises an error if the packet is
            % invalid.
            % 
            % Arguments:
            %  - packet: Bcap response packet to decode. [array of uint]
            %  - serial: Serial number of the request packet. [int]
            %
            % Returns:
            %  - Variables included in the response packet. [array]
            
            % SOH check
            if packet(1) ~= 1
                throw(MException('BCAP:InvalidPacket', 'SOH not found'));
            end
            packet = packet(2:end);
            
            %Remove packet length information
            packet = packet(5:end);
            
            % Serial check
            if typecast(uint8(packet(1:2)), 'uint16') ~= serial
                throw(MException('BCAP:InvalidPacket', 'Bad serial'));
            end
            packet = packet(3:end);
            
            % Version check
            if typecast(uint8(packet(1:2)), 'uint16') ~= 1
                throw(MException('BCAP:InvalidPacket', 'Bad version'));
            end
            packet = packet(3:end);
            
            % check command state
            state = typecast(uint8(packet(1:4)), 'uint32');
            if state ~= Bcap.S_OK
                if isKey(Bcap.ERRORS, state)
                    error = Bcap.ERRORS(state);
                else
                    error = 'Unknown error';
                end
                message = ['Robot returned the error: ' error];
                throw(MException('BCAP:ErrorCode', message));
            end
            packet = packet(5:end);
            
            nargs = typecast(uint8(packet(1:2)), 'uint16');
            packet = packet(3:end);
            args = [];
            % decode every argument
            for i=1:1:nargs
                vtype = typecast(uint8(packet(5:6)), 'uint16');
                arr_len = typecast(uint8(packet(7:10)), 'uint32');
                packet = packet(11:end);
                if vtype == Bcap.VT_BSTR
                    vlen = typecast(uint8(packet(1:4)), 'uint32');
                    packet = packet(5:end);    
                else
                    % If vtype is greater than 8192 the argument is an
                    % array.
                    if vtype > 8192
                        vtype = vtype - 8192;
                    end
                    vlen = Bcap.VLENGTH(vtype) * arr_len;
                end
                args = [args, obj.bytes_to_arg(vtype, packet(1:vlen))]; 
                packet = packet(vlen+1:end);
            end
            %Mode check
            if packet(1) ~= 0
                throw(MException('BCAP:InvalidPacket', 'Compressed mode not supported'));
            end
            
            % EOT check
            if packet(2) ~= 4
                throw(MException('BCAP:InvalidPacket', 'EOT not found'));
            end
            return
        end
    end
end
