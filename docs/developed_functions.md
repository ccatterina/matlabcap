## BCAP developed functions
-  **Service_Start** (Function ID: 1)

   `function service_start(obj)`

-  **Service_Stop** (Function ID: 2)

   `function service_start(obj)`

- **Controller_Connect** (Function ID: 3)

  `function controller = controller_connect(obj)`

  Returns:
    - *controller*: Controller handle. [int16]

- **Controller_Disconnect** (Function ID: 4)

  `function controller_disconnect(obj, controller)`

  Arguments:
    - *controller*: Controller handle. [int16] 

 - **Controller_GetRobot**  (Function ID: 7)

   `function robot_handle = get_robot(obj, controller)`

    Returns:
      - *robot_handle*: Robot handle. [int32]

- **Robot_GetVariable** (Function ID: 62)

  `function var_handle = robot_get_variable(obj, var_name)`

  Arguments:
    - *var_name*: Name of the robot variable. [string]
  Returns:
    - *var_handle*: Handle of the variable. [int32]

- **Robot_Speed**  (Function ID: 74)

  `function change_speed(obj, speed)`

  Arguments:
    - *speed*: Speed value to set (0-100). [single]

- **Robot_GetValue** (Function ID: 101)

  `function value = robot_get_value(obj, var_handle)`

  Arguments:
    - *var_handle*: The variable handle. [int32]
  Returns:
    - *value*: The current value of the variable. [depends on variable type]

- **Robot_PutValue** (Function ID: 102)

  `function robot_put_value(obj, var_handle, var_type, value)`

  Arguments:
    - *var_handle*: The variable handle. [int32]
    - *var_type*: Variable type (See Bcap properties) [int16] 
    - *value*: Value to write. [depends on variable type]

- **Variable_Release** (Function ID: 111)

  `function release_var(obj, var_handle)`

  Arguments:
    - *var_handle*: The variable handle. [int32]

## Other non-standard functions
- **`function connect(obj)`**

  Description: Opens the socket, starts service, gets controller and robot handles.
- **`function disconnect(obj)`**

  Description: Disconnects controller, stops service and closes the socket.
- **`function change_ext_speed(obj, speed)`**

  Arguments:
    - *speed*: Speed value to set (0-100). [single]

  Description: Change the external speed of the robot.  
- **`function speed = current_ext_speed(obj)`**

  Returns:
   - *speed*: Current external speed value. [single]
- **`function takearm(obj)`**

  Description: Take the control of the arm, this command may fail if someone else already has the arm control.  
- **`function pos = get_current_pos(obj)`**

  Returns:
   - *pos*: The current position of the robot. [array of 3 int (x, y, z)]
